import express from "express"
import { readFile } from 'node:fs/promises'

const app = express()
app.use(express.urlencoded({ extended: false }))

app.set("view engine", "ejs")
app.set("views", "views")

app.post("/quizz/start", async (req, res) => {
    let data = await readFile('countries.json')
    data = JSON.parse(data)
    res.json(data)
})

app.get("/question/:id", (req, res) => {
    res.render("newQuestion", { desiredInfo: "la capitale", tip: "France", id: req.params.id })
})

app.post("/chooseAnswerType/:id", (req, res) => {

    res.send(req.body.answerType)
})

app.get("/dashboard/:name", (req, res) => {
    res.render("index", { name: req.params.name })
})
app.get("/test", (req, res) => {
    res.send("OK")
})

app.get("/hello/:name", (req, res) => {
    res.send("hello " + req.params?.name + " !");
})

app.listen(4000, () => {
    console.log("serveur démarré sur le port 4000...");
})